# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  arr = (nums[0]..nums[-1]).to_a
  arr - nums
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.
require 'byebug'
def base2to10(binary)
  array = []
  binary.each_char.with_index do |num, idx|
    array << [num.to_i * (2**idx)]
  end
  array.flatten.reduce(:+)
end
#confused why the longer test cases work fine, but
#with '10' and '1100' it doesn't


class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
   to_return = {}
   self.each do |k, v|
     to_return[k] = v if prc.call(k, v)
   end
   to_return
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

    def my_merge(hash, &prc)
      to_return = {}
      self.each do |key, val|

          if hash.has_key?(key)
            if prc.nil?
              to_return[key] = hash[key]
            else
              to_return[key] = prc.call(key, val, hash[key])
            end

          else
            to_return[key] = val
          end
      end
      hash.each do |k, v|
        if !self.has_key?(k)
          to_return[k] = v
        end
      end
      to_return
    end
end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  # lucas = {
  #   0 => 2
  #   1 => 1
  # }
  # i = 0
  # until lucas[n]
  #   lucas << (lucas[i] + lucas[i + 1])
  # end
  # lucas[n]
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)

  array = string.split(//)

  i = 0
  palindrome_sets = []
  while i <= array.length

    x = 0
    while x <= array.length
      if palindrome?(array.slice(i, x))
        palindrome_sets.push(array.slice(i, x))
      end
      x += 1
    end
    i += 1
  end

  x = 0
  longest = palindrome_sets[0]
  while x < palindrome_sets.length
    if palindrome_sets[x].length > longest.length
      longest = palindrome_sets[x]
    end
    x += 1
  end
  return longest.size unless longest.size == 1
  false
end

def palindrome?(string)
  i = 0
  while i < string.length
    if string[i] != string[(string.length - 1) - i]
      return false
    end
  i += 1
  end
  true
end
